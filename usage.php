<?php

require 'RBDBS.php';

$details = array(	"host" => "localhost", 
					"database" => "mydatabase", 
					"user"=> "burrito", 
					"password"=> "sargents" );

$db = new Database( $details );

/*
 * The sample database table being used here is just some garbage
 * that is only to provide a small portion of data to manipulate
 * with this script.
 * An export of this table is currently included as 'people.sql'
 * */

// To startor open the database connection
// $db->Open();

// To start the database connection and get a handler for
// the table object while doing so:
// echo "open( 'tableName' ) function example:<br>";
// $table = $db->Open( "myTable" );
// echo $table->name;

// To set the table name to be used and create the table \
// handler.
// Note: Does not open database connection.  So, you will have
// to use Open() before executing any actual transactions
// echo " using \$table = \$db->Open( \"myTable\" );<br>";
// $table = $db->Table( "thisTable" );
// echo $table->name;

// To perform an insert using a table handler:
/*
$data = array("fname"=>"Bill", "lname"=>"Doe", "age"=>155);
$db->Open();
$table = $db->Table( "people" );
$ins = $table->Insert( $data );
echo $ins;
*/

// To perform an insert using a table handler:
// This example creates the table handler in Open()
/*
$data = array("fname"=>"Frank", "lname"=>"Joplin", "age"=>66);
$table = $db->Open( "people" );
$ins = $table->Insert( $data );
echo $ins;
*/


/*
$table = $db->Open( "people" );
echo "<br> Using operators: <br> ";
$a = array("fname", "!=", "Bill");
$b = array("lname", "=", "Doe");
$ghosts = array($a, $b);
$query = $table->All( $ghosts );
echo $query;
*/

/*
$table = $db->Open( "people" );
echo "<br> Just looking for rows with certain values: <br> ";
//$ghosts = array($a, $b);
$conditions = array("fname"=>"Bill", "lname"=>"Doe");
$query = $table->All( $conditions );
echo $query;
*/

// A very simple selectAll statement using the table's All() function
/*
$details = array("fname"=>"Bill", "age"=>115);
$table = $db->Open( "people" );
$fj = $table->All( $details );
var_dump( $fj );
*/

// A more complex selectAll statement using the All() function
/*
$cond_0 = array("age", "!=", 155);
$cond_1 = array("lname", "!=", "Dick");
$details = array($cond_0, $cond_1);
$table = $db->Open( "people" );
$fj = $table->All( $details );
var_dump( $fj );
*/

// Just selecting all the content of the table
/*
$table = $db->Open( "people" );
$fj = $table->All();
var_dump( $fj );
*/

// Select specific values from rows
/*
$table = $db->Open( "people" );
$fetch = $table->Fetch( array("fname", "lname") );
$result = $fetch->Where( array("fname"=>"freddie") );
var_dump( $result );
*/

// When you need to update fields
$field = array("id"=>4);
$data = array("fname"=>"Hank", "lname"=>"Hands", "age"=>70);

$table = $db->Open( "people" );
$update = $table->Update($field, $data);
echo $update;

?>
