<?php

class PDO_Database_Connection{
	
	private $host;
	private $database;
	private $user;
	private $password;
		
	public $dbh;
	public $error;
	public $status;

	public function __construct( $args ){
		
		foreach( $args as $key => $value ){
		
			$this->$key = $value;
			
		}
		
		try {
			$this->dbh = new PDO("mysql:host=$this->host;dbname=$this->database", $this->user, $this->password);
			/*** echo a message saying we have connected ***/
			//echo 'Connected to database';
			$this->status = "connected";
			}
		catch(PDOException $e)
			{
			//echo $e->getMessage();
			$this->error = $e->getMessage();
			}
			
	}
	
}

class db_info {
	
	public $PDO_Availability;
	public $MySQLi_Availability;
	
	public function __construct(){
	
		$this->PDO_Available();
		$this->MySQLi_Available();
		
	}
	
	public function PDO_Available(){
		
		if (!defined('PDO::ATTR_DRIVER_NAME')) {
			$this->PDO_Availability = false;
		}else{
			$this->PDO_Availability = true;
		}
		
	}
	
	public function MySQLi_Available(){
		if (function_exists('mysqli_connect')) {
		  //mysqli is installed
		  $this->MySQLi_Availability = true;
		}else{
		  $this->MySQLi_Availability = false;
		}
		
	}
	
}	

class _database_core_ {
	
	private $host; private $database; private $user; private $password;
	
	public $dbh; public $table; public $query_string;
	
	public function __construct(){
		
		$this->table = false;
		
	}
	
}

$core = new _database_core_();

class SelectAll {
	
	public $params;
	
	public $fields;
	
	private $condition_string;
	
	public $results;

	public function __construct( $args ){ $this->params = $args; }
	
	public function simpleSetup(){
		
		global $core;
		
		$this->fields = array();
		
		$this->condition_string = "SELECT * FROM " . $core->table->name . " WHERE ";
		
		foreach( $this->params as $key => $value ){
			
			$this->condition_string .= $key . " = :" . $key . " AND ";
			
		}
		
		$this->condition_string = rtrim( $this->condition_string, ' AND ');
	
		$statement = $core->dbh->prepare( $this->condition_string );
		$statement->execute( $this->params );
		$this->results = $statement->fetchAll();
		
	}
	
	public function complexSetup(){
		
		global $core;
		
		$this->fields = array();
		
		$this->condition_string = "SELECT * FROM " . $core->table->name . " WHERE ";
	
		foreach( $this->params as $parameter ){
			
			$this->condition_string .= $parameter[0] . " " . $parameter[1] . " '" . $parameter[2] . "' AND ";
			
		}
		
		$this->condition_string = rtrim( $this->condition_string, ' AND ');
		$statement = $core->dbh->prepare( $this->condition_string );
		$statement->execute();
		$this->results = $statement->fetchAll();
		
	}
	
	public function noSetup(){
	
		global $core;
		
		$this->condition_string = "SELECT * FROM " . $core->table->name;
		$statement = $core->dbh->prepare( $this->condition_string );
		$statement->execute();
		$this->results = $statement->fetchAll();
		
	}
	
	public function readString(){
	
		return $this->condition_string;
		
	}
	
	public function getResults(){
	
		return $this->results;
		
	}
	
	
}

class Insert {
	
	public $tableFields;
	
	public $params;
	
	public $DATA;
	
	public $ID;
	
	public function __construct( $args ){
		
		global $core;
		
		if( is_array( $args ) ){
	
			$this->tableFields = array();
			
			if( $core->table ){
				
				$core->query_string = "INSERT INTO " . $core->table->name . "(";
				
				$this->valuesAndVars( $args );
				
				$core->query_string .= $this->tableFields.") VALUES	(".$this->params.")";
				
				$this->DATA = $args;
				
				$this->process();
				
			}else{
			
				// specify error here
				
			}			
	
		}else{
			
			// report or specify error here
			
		}
		

		//$core->query_string;
		
	}
	
	public function READER(){
		
		global $core;
		return $core->query_string;
		
	}
	
	private function valuesAndVars( $args ){
		
		$str = ""; $stri = "";
		
		foreach( $args as $key => $value ){
			
			$str .= $key.", ";	$stri .= ":".$key.", ";
			
		}
	
		$this->tableFields = rtrim( trim( $str ), ',');
		$this->params = rtrim( trim( $stri ), ',');
		
	}
	
	private function process(){
		global $core;
	
		$statement = $core->dbh->prepare( $core->query_string );
		$statement->execute( $this->DATA );
		$this->ID = $core->dbh->lastInsertId();
		
	}
	
}

class Fetch {
	
	public $params;
	public $DATA;
	public $result;
	public $query_string;
	
	public function __construct( $args = false ){
		
		if( is_array( $args ) ){
			
			$this->params = $args;
			
		}
		
	}
	
	public function Where( $args ){
		global $core;
		
		$this->query_string = "SELECT ";
		
		foreach( $this->params as $value ){
			
			$this->query_string .= "`" . $value . "`, ";
			
		}
		
		$this->query_string = rtrim( trim( $this->query_string ), ',');
		$this->query_string .= " FROM " . $core->table->name . " WHERE ";
		
		if( is_array( $args ) ){

			if( array_key_exists(0, $args) ){	
				// this part really needs to be worked out right
				// before expecting to be able to get use from it
				// this is why it is commented out.
				
				/*
				$this->DATA = array();
					
					foreach( $args as $key => $value ){
						
						$this->DATA[$key] = $value;
						
						$this->query_string .= $key . " = :" . $key . " AND ";
						
					}
					
					$this->query_string = rtrim( trim( $this->query_string ), 'AND');
					
					$statement = $core->dbh->prepare( $this->query_string );
				*/
			}else{

				$this->DATA = array();
				
				foreach( $args as $key => $value ){
					
					$this->DATA[$key] = $value;
					
					$this->query_string .= $key . " = :" . $key . " AND ";
					
				}
				
				$this->query_string = rtrim( trim( $this->query_string ), 'AND');
				
				$statement = $core->dbh->prepare( $this->query_string );

				$statement->execute( $this->DATA );
				
				return $statement->fetchAll();		

			}
			
		}
		
	}

}

class Update {
	
	public $status;
	
	public $select_fields;
	
	public $data_fields;
	
	private $query_string;

	public function __construct( $selection_info, $data ){

		$this->select_fields = $selection_info;
		
		$this->data_fields = $data;
		
		$this->QueryString();
		
		$this->Execute();
		
	}
	
	private function QueryString(){
		
		global $core;

		$this->query_string = "UPDATE " . $core->table->name . " SET ";
		
		foreach( $this->data_fields as $key => $value ){
			
			$this->query_string .= $key . " = :" . $key . ", ";
			
		}
		
		$this->query_string = rtrim( trim( $this->query_string ), ',');
		
		$this->query_string .= " WHERE ";
		
		foreach( $this->select_fields as $key => $value ){
			
			$this->query_string .= $key . " = :" . $key . " AND ";
			
		}
		
		$this->query_string = rtrim( trim( $this->query_string ), 'AND');
		
		
	}
	
	public function Execute(){
	
		global $core;
		
		$statement = $core->dbh->prepare( $this->query_string );
		
		foreach( $this->data_fields as $k => $v ){
			
			$statement->bindParam(':'.$k, $this->data_fields[$k]);
			
		}

		foreach( $this->select_fields as $key => $value ){
			
			$statement->bindParam(':'.$key, $this->select_fields[$key]);
			
		}
		
		if( $statement->execute() ){
		
			$this->status = true;
			
		}else{
			
			$this->status = false;
			
		}
		
	}
	
	public function info(){
	
		return $this->query_string;
		
	}
	
	
}

class Table {
	
	public $name;
	public $status;
	
	public function __construct( $name ){
		
		$this->name = $name;
		
	}
	
	public function Insert( $args ){
		
		$ins = new Insert( $args );
		return $ins->ID;
		
	}
	
	public function All( $args = false ){
		
		if( !$args ){
			
			$gh = new SelectAll( false );
			$gh->noSetup();
			return $gh->results;			 
			
		}else if( is_array( $args ) ){
			
			if( array_key_exists(0, $args) ){
			/*
			 * This is what we will expect to use when we need to use
			 * conditional statements that evaluate more than equality
			 * array("field", "!=", "standard")
			 * 
			 * */
			 //$this->status = count( $args[0] )." going to look for an operator";
				$gh = new SelectAll( $args );
				$gh->complexSetup();
				return $gh->results;			 
			
			}else{
			/*
			 * This is the block that gets used when all conditions
			 * of the select statement are comparing equality
			 * 
			 * */
				$gh = new SelectAll( $args );
				$gh->simpleSetup();
				return $gh->results;
				
			}
			
		}
		
	}
	
	public function Fetch( $args = false ){
		
		if( !$args ){
		
		}else if( is_array( $args ) ){

			return new Fetch( $args );
			
		}
		
	}
	
	public function Update( $selection_info, $data ){
		
		$update = new Update( $selection_info, $data );
		return $update->status;
		
	}
	
}

class Database {
	
	public $internal_data;
	private $CONNECTED_TO_DB;
	private $host;
	private $database;
	private $user;
	private $password;
		
	public $dbh;
	public $error;
	public $status;
	
	public $table;
	
	public function __construct( $args ){
		
		$this->CONNECTED_TO_DB = false;
		
		if( is_array( $args ) ){
			
			foreach( $args as $key => $value ){
			
				$this->$key = $value;
				
			}

		}else{
			
			
		}
		
	}
	
	protected function connect_to_database(){
	
		global $core;
		
		$details = array(	"host" => $this->host,
							"database" => $this->database,
							"user" => $this->user,
							"password" => $this->password );
		
		$db = new PDO_Database_Connection( $details );
		
		if( $db->status == "connected" ){
			
			$this->dbh = $db->dbh;
			$core->dbh = $this->dbh;
			$this->CONNECTED_TO_DB = true;
			
		}else{
			
			$this->error = "Database Connected Failed.";
			
		}
		
	}
	
	protected function db_conn(){
		
		if( !$this->CONNECTED_TO_DB ){ $this->connect_to_database(); }
		
	}
	
	public function Open( $arg = false ){

		global $core;
			
		$this->db_conn();
		
		if( $arg ){
		
			$this->table = $arg;
			$core->table = new Table( $arg );
			return $core->table;
		 	
		}
		
	}
	
	public function check(){
	
		if( !$this->CONNECTED_TO_DB ){
			return "not connected to database";
		}else{
			return "connected to database";
		}
		
	}
	
	public function Table( $arg = false ){

		global $core;
		
		if( !$arg ){
			
			//return
			
		}else{
			
			$this->table = $arg;
			$core->table = new Table( $arg );
			return $core->table;			
			
		}
		

		
	}

	
	
}


?>



